package main

import (
	"context"
	"log"

	"gitlab.com/medium_clone/medium_notification_service_with_kafka/config"
	"gitlab.com/medium_clone/medium_notification_service_with_kafka/events"
)

func main() {
	cfg := config.Load(".")

	pubsub, err := events.New(cfg)
	if err != nil {
		log.Fatalf("failed to open pubsub %v", err)
	}

	pubsub.Run(context.Background())
}
