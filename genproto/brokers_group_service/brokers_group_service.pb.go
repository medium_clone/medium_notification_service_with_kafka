// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0-devel
// 	protoc        v3.14.0
// source: brokers_group_service.proto

package brokers_group_service

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type BrokerInfo struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id              int64  `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	FirstName       string `protobuf:"bytes,2,opt,name=first_name,json=firstName,proto3" json:"first_name,omitempty"`
	LastName        string `protobuf:"bytes,3,opt,name=last_name,json=lastName,proto3" json:"last_name,omitempty"`
	Username        string `protobuf:"bytes,4,opt,name=username,proto3" json:"username,omitempty"`
	ProfileImageUrl string `protobuf:"bytes,5,opt,name=profile_image_url,json=profileImageUrl,proto3" json:"profile_image_url,omitempty"`
	Email           string `protobuf:"bytes,6,opt,name=email,proto3" json:"email,omitempty"`
}

func (x *BrokerInfo) Reset() {
	*x = BrokerInfo{}
	if protoimpl.UnsafeEnabled {
		mi := &file_brokers_group_service_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BrokerInfo) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BrokerInfo) ProtoMessage() {}

func (x *BrokerInfo) ProtoReflect() protoreflect.Message {
	mi := &file_brokers_group_service_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BrokerInfo.ProtoReflect.Descriptor instead.
func (*BrokerInfo) Descriptor() ([]byte, []int) {
	return file_brokers_group_service_proto_rawDescGZIP(), []int{0}
}

func (x *BrokerInfo) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *BrokerInfo) GetFirstName() string {
	if x != nil {
		return x.FirstName
	}
	return ""
}

func (x *BrokerInfo) GetLastName() string {
	if x != nil {
		return x.LastName
	}
	return ""
}

func (x *BrokerInfo) GetUsername() string {
	if x != nil {
		return x.Username
	}
	return ""
}

func (x *BrokerInfo) GetProfileImageUrl() string {
	if x != nil {
		return x.ProfileImageUrl
	}
	return ""
}

func (x *BrokerInfo) GetEmail() string {
	if x != nil {
		return x.Email
	}
	return ""
}

type BrokersGroupResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id          int64       `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name        string      `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	AdminInfo   *BrokerInfo `protobuf:"bytes,3,opt,name=admin_info,json=adminInfo,proto3" json:"admin_info,omitempty"`
	GroupInfo   string      `protobuf:"bytes,4,opt,name=group_info,json=groupInfo,proto3" json:"group_info,omitempty"`
	PhoneNumber string      `protobuf:"bytes,5,opt,name=phone_number,json=phoneNumber,proto3" json:"phone_number,omitempty"`
	Email       string      `protobuf:"bytes,6,opt,name=email,proto3" json:"email,omitempty"`
	Address     string      `protobuf:"bytes,7,opt,name=address,proto3" json:"address,omitempty"`
	ImageUrl    string      `protobuf:"bytes,8,opt,name=image_url,json=imageUrl,proto3" json:"image_url,omitempty"`
	Level       string      `protobuf:"bytes,9,opt,name=level,proto3" json:"level,omitempty"`
	CreatedAt   string      `protobuf:"bytes,10,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
}

func (x *BrokersGroupResponse) Reset() {
	*x = BrokersGroupResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_brokers_group_service_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BrokersGroupResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BrokersGroupResponse) ProtoMessage() {}

func (x *BrokersGroupResponse) ProtoReflect() protoreflect.Message {
	mi := &file_brokers_group_service_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BrokersGroupResponse.ProtoReflect.Descriptor instead.
func (*BrokersGroupResponse) Descriptor() ([]byte, []int) {
	return file_brokers_group_service_proto_rawDescGZIP(), []int{1}
}

func (x *BrokersGroupResponse) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *BrokersGroupResponse) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *BrokersGroupResponse) GetAdminInfo() *BrokerInfo {
	if x != nil {
		return x.AdminInfo
	}
	return nil
}

func (x *BrokersGroupResponse) GetGroupInfo() string {
	if x != nil {
		return x.GroupInfo
	}
	return ""
}

func (x *BrokersGroupResponse) GetPhoneNumber() string {
	if x != nil {
		return x.PhoneNumber
	}
	return ""
}

func (x *BrokersGroupResponse) GetEmail() string {
	if x != nil {
		return x.Email
	}
	return ""
}

func (x *BrokersGroupResponse) GetAddress() string {
	if x != nil {
		return x.Address
	}
	return ""
}

func (x *BrokersGroupResponse) GetImageUrl() string {
	if x != nil {
		return x.ImageUrl
	}
	return ""
}

func (x *BrokersGroupResponse) GetLevel() string {
	if x != nil {
		return x.Level
	}
	return ""
}

func (x *BrokersGroupResponse) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

type BrokersGroupImageUploadRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	BrokersGroupId int64  `protobuf:"varint,1,opt,name=brokers_group_id,json=brokersGroupId,proto3" json:"brokers_group_id,omitempty"`
	ImageUrl       string `protobuf:"bytes,2,opt,name=image_url,json=imageUrl,proto3" json:"image_url,omitempty"`
}

func (x *BrokersGroupImageUploadRequest) Reset() {
	*x = BrokersGroupImageUploadRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_brokers_group_service_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BrokersGroupImageUploadRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BrokersGroupImageUploadRequest) ProtoMessage() {}

func (x *BrokersGroupImageUploadRequest) ProtoReflect() protoreflect.Message {
	mi := &file_brokers_group_service_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BrokersGroupImageUploadRequest.ProtoReflect.Descriptor instead.
func (*BrokersGroupImageUploadRequest) Descriptor() ([]byte, []int) {
	return file_brokers_group_service_proto_rawDescGZIP(), []int{2}
}

func (x *BrokersGroupImageUploadRequest) GetBrokersGroupId() int64 {
	if x != nil {
		return x.BrokersGroupId
	}
	return 0
}

func (x *BrokersGroupImageUploadRequest) GetImageUrl() string {
	if x != nil {
		return x.ImageUrl
	}
	return ""
}

type BrokersGroup struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id          int64  `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name        string `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	AdminId     int64  `protobuf:"varint,3,opt,name=admin_id,json=adminId,proto3" json:"admin_id,omitempty"`
	GroupInfo   string `protobuf:"bytes,4,opt,name=group_info,json=groupInfo,proto3" json:"group_info,omitempty"`
	PhoneNumber string `protobuf:"bytes,5,opt,name=phone_number,json=phoneNumber,proto3" json:"phone_number,omitempty"`
	Email       string `protobuf:"bytes,6,opt,name=email,proto3" json:"email,omitempty"`
	Address     string `protobuf:"bytes,7,opt,name=address,proto3" json:"address,omitempty"`
	ImageUrl    string `protobuf:"bytes,8,opt,name=image_url,json=imageUrl,proto3" json:"image_url,omitempty"`
	Level       string `protobuf:"bytes,9,opt,name=level,proto3" json:"level,omitempty"`
	CreatedAt   string `protobuf:"bytes,10,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
}

func (x *BrokersGroup) Reset() {
	*x = BrokersGroup{}
	if protoimpl.UnsafeEnabled {
		mi := &file_brokers_group_service_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BrokersGroup) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BrokersGroup) ProtoMessage() {}

func (x *BrokersGroup) ProtoReflect() protoreflect.Message {
	mi := &file_brokers_group_service_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BrokersGroup.ProtoReflect.Descriptor instead.
func (*BrokersGroup) Descriptor() ([]byte, []int) {
	return file_brokers_group_service_proto_rawDescGZIP(), []int{3}
}

func (x *BrokersGroup) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *BrokersGroup) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *BrokersGroup) GetAdminId() int64 {
	if x != nil {
		return x.AdminId
	}
	return 0
}

func (x *BrokersGroup) GetGroupInfo() string {
	if x != nil {
		return x.GroupInfo
	}
	return ""
}

func (x *BrokersGroup) GetPhoneNumber() string {
	if x != nil {
		return x.PhoneNumber
	}
	return ""
}

func (x *BrokersGroup) GetEmail() string {
	if x != nil {
		return x.Email
	}
	return ""
}

func (x *BrokersGroup) GetAddress() string {
	if x != nil {
		return x.Address
	}
	return ""
}

func (x *BrokersGroup) GetImageUrl() string {
	if x != nil {
		return x.ImageUrl
	}
	return ""
}

func (x *BrokersGroup) GetLevel() string {
	if x != nil {
		return x.Level
	}
	return ""
}

func (x *BrokersGroup) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

type ChangeBrokerGroup struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id          int64  `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Name        string `protobuf:"bytes,2,opt,name=name,proto3" json:"name,omitempty"`
	GroupInfo   string `protobuf:"bytes,3,opt,name=group_info,json=groupInfo,proto3" json:"group_info,omitempty"`
	PhoneNumber string `protobuf:"bytes,4,opt,name=phone_number,json=phoneNumber,proto3" json:"phone_number,omitempty"`
	Address     string `protobuf:"bytes,5,opt,name=address,proto3" json:"address,omitempty"`
}

func (x *ChangeBrokerGroup) Reset() {
	*x = ChangeBrokerGroup{}
	if protoimpl.UnsafeEnabled {
		mi := &file_brokers_group_service_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ChangeBrokerGroup) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ChangeBrokerGroup) ProtoMessage() {}

func (x *ChangeBrokerGroup) ProtoReflect() protoreflect.Message {
	mi := &file_brokers_group_service_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ChangeBrokerGroup.ProtoReflect.Descriptor instead.
func (*ChangeBrokerGroup) Descriptor() ([]byte, []int) {
	return file_brokers_group_service_proto_rawDescGZIP(), []int{4}
}

func (x *ChangeBrokerGroup) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *ChangeBrokerGroup) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *ChangeBrokerGroup) GetGroupInfo() string {
	if x != nil {
		return x.GroupInfo
	}
	return ""
}

func (x *ChangeBrokerGroup) GetPhoneNumber() string {
	if x != nil {
		return x.PhoneNumber
	}
	return ""
}

func (x *ChangeBrokerGroup) GetAddress() string {
	if x != nil {
		return x.Address
	}
	return ""
}

type CreateBrokersGroupRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name        string `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
	AdminId     int64  `protobuf:"varint,2,opt,name=admin_id,json=adminId,proto3" json:"admin_id,omitempty"`
	GroupInfo   string `protobuf:"bytes,3,opt,name=group_info,json=groupInfo,proto3" json:"group_info,omitempty"`
	PhoneNumber string `protobuf:"bytes,4,opt,name=phone_number,json=phoneNumber,proto3" json:"phone_number,omitempty"`
	Email       string `protobuf:"bytes,5,opt,name=email,proto3" json:"email,omitempty"`
	Address     string `protobuf:"bytes,6,opt,name=address,proto3" json:"address,omitempty"`
}

func (x *CreateBrokersGroupRequest) Reset() {
	*x = CreateBrokersGroupRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_brokers_group_service_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateBrokersGroupRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateBrokersGroupRequest) ProtoMessage() {}

func (x *CreateBrokersGroupRequest) ProtoReflect() protoreflect.Message {
	mi := &file_brokers_group_service_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateBrokersGroupRequest.ProtoReflect.Descriptor instead.
func (*CreateBrokersGroupRequest) Descriptor() ([]byte, []int) {
	return file_brokers_group_service_proto_rawDescGZIP(), []int{5}
}

func (x *CreateBrokersGroupRequest) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *CreateBrokersGroupRequest) GetAdminId() int64 {
	if x != nil {
		return x.AdminId
	}
	return 0
}

func (x *CreateBrokersGroupRequest) GetGroupInfo() string {
	if x != nil {
		return x.GroupInfo
	}
	return ""
}

func (x *CreateBrokersGroupRequest) GetPhoneNumber() string {
	if x != nil {
		return x.PhoneNumber
	}
	return ""
}

func (x *CreateBrokersGroupRequest) GetEmail() string {
	if x != nil {
		return x.Email
	}
	return ""
}

func (x *CreateBrokersGroupRequest) GetAddress() string {
	if x != nil {
		return x.Address
	}
	return ""
}

type GetAllBrokersGroupsResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	BrokersGroup []*BrokersGroup `protobuf:"bytes,1,rep,name=brokers_group,json=brokersGroup,proto3" json:"brokers_group,omitempty"`
	Count        int64           `protobuf:"varint,2,opt,name=count,proto3" json:"count,omitempty"`
}

func (x *GetAllBrokersGroupsResponse) Reset() {
	*x = GetAllBrokersGroupsResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_brokers_group_service_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAllBrokersGroupsResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAllBrokersGroupsResponse) ProtoMessage() {}

func (x *GetAllBrokersGroupsResponse) ProtoReflect() protoreflect.Message {
	mi := &file_brokers_group_service_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAllBrokersGroupsResponse.ProtoReflect.Descriptor instead.
func (*GetAllBrokersGroupsResponse) Descriptor() ([]byte, []int) {
	return file_brokers_group_service_proto_rawDescGZIP(), []int{6}
}

func (x *GetAllBrokersGroupsResponse) GetBrokersGroup() []*BrokersGroup {
	if x != nil {
		return x.BrokersGroup
	}
	return nil
}

func (x *GetAllBrokersGroupsResponse) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

type BrokersGroupIdRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id int64 `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *BrokersGroupIdRequest) Reset() {
	*x = BrokersGroupIdRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_brokers_group_service_proto_msgTypes[7]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *BrokersGroupIdRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*BrokersGroupIdRequest) ProtoMessage() {}

func (x *BrokersGroupIdRequest) ProtoReflect() protoreflect.Message {
	mi := &file_brokers_group_service_proto_msgTypes[7]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use BrokersGroupIdRequest.ProtoReflect.Descriptor instead.
func (*BrokersGroupIdRequest) Descriptor() ([]byte, []int) {
	return file_brokers_group_service_proto_rawDescGZIP(), []int{7}
}

func (x *BrokersGroupIdRequest) GetId() int64 {
	if x != nil {
		return x.Id
	}
	return 0
}

type GetAllBrokersGroupsRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Page       int64  `protobuf:"varint,1,opt,name=page,proto3" json:"page,omitempty"`
	Limit      int64  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	Search     string `protobuf:"bytes,3,opt,name=search,proto3" json:"search,omitempty"`
	SortByDate string `protobuf:"bytes,4,opt,name=sort_by_date,json=sortByDate,proto3" json:"sort_by_date,omitempty"`
}

func (x *GetAllBrokersGroupsRequest) Reset() {
	*x = GetAllBrokersGroupsRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_brokers_group_service_proto_msgTypes[8]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetAllBrokersGroupsRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetAllBrokersGroupsRequest) ProtoMessage() {}

func (x *GetAllBrokersGroupsRequest) ProtoReflect() protoreflect.Message {
	mi := &file_brokers_group_service_proto_msgTypes[8]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetAllBrokersGroupsRequest.ProtoReflect.Descriptor instead.
func (*GetAllBrokersGroupsRequest) Descriptor() ([]byte, []int) {
	return file_brokers_group_service_proto_rawDescGZIP(), []int{8}
}

func (x *GetAllBrokersGroupsRequest) GetPage() int64 {
	if x != nil {
		return x.Page
	}
	return 0
}

func (x *GetAllBrokersGroupsRequest) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *GetAllBrokersGroupsRequest) GetSearch() string {
	if x != nil {
		return x.Search
	}
	return ""
}

func (x *GetAllBrokersGroupsRequest) GetSortByDate() string {
	if x != nil {
		return x.SortByDate
	}
	return ""
}

var File_brokers_group_service_proto protoreflect.FileDescriptor

var file_brokers_group_service_proto_rawDesc = []byte{
	0x0a, 0x1b, 0x62, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x5f, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x5f,
	0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x08, 0x67,
	0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x1b, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x65, 0x6d, 0x70, 0x74, 0x79, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x22, 0xb6, 0x01, 0x0a, 0x0a, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x49,
	0x6e, 0x66, 0x6f, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52,
	0x02, 0x69, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x66, 0x69, 0x72, 0x73, 0x74, 0x5f, 0x6e, 0x61, 0x6d,
	0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x66, 0x69, 0x72, 0x73, 0x74, 0x4e, 0x61,
	0x6d, 0x65, 0x12, 0x1b, 0x0a, 0x09, 0x6c, 0x61, 0x73, 0x74, 0x5f, 0x6e, 0x61, 0x6d, 0x65, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x6c, 0x61, 0x73, 0x74, 0x4e, 0x61, 0x6d, 0x65, 0x12,
	0x1a, 0x0a, 0x08, 0x75, 0x73, 0x65, 0x72, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x08, 0x75, 0x73, 0x65, 0x72, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x2a, 0x0a, 0x11, 0x70,
	0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x5f, 0x69, 0x6d, 0x61, 0x67, 0x65, 0x5f, 0x75, 0x72, 0x6c,
	0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0f, 0x70, 0x72, 0x6f, 0x66, 0x69, 0x6c, 0x65, 0x49,
	0x6d, 0x61, 0x67, 0x65, 0x55, 0x72, 0x6c, 0x12, 0x14, 0x0a, 0x05, 0x65, 0x6d, 0x61, 0x69, 0x6c,
	0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x65, 0x6d, 0x61, 0x69, 0x6c, 0x22, 0xb3, 0x02,
	0x0a, 0x14, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x33, 0x0a, 0x0a, 0x61, 0x64,
	0x6d, 0x69, 0x6e, 0x5f, 0x69, 0x6e, 0x66, 0x6f, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x14,
	0x2e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72,
	0x49, 0x6e, 0x66, 0x6f, 0x52, 0x09, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x49, 0x6e, 0x66, 0x6f, 0x12,
	0x1d, 0x0a, 0x0a, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x5f, 0x69, 0x6e, 0x66, 0x6f, 0x18, 0x04, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x09, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x49, 0x6e, 0x66, 0x6f, 0x12, 0x21,
	0x0a, 0x0c, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x5f, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x18, 0x05,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x4e, 0x75, 0x6d, 0x62, 0x65,
	0x72, 0x12, 0x14, 0x0a, 0x05, 0x65, 0x6d, 0x61, 0x69, 0x6c, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x05, 0x65, 0x6d, 0x61, 0x69, 0x6c, 0x12, 0x18, 0x0a, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65,
	0x73, 0x73, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73,
	0x73, 0x12, 0x1b, 0x0a, 0x09, 0x69, 0x6d, 0x61, 0x67, 0x65, 0x5f, 0x75, 0x72, 0x6c, 0x18, 0x08,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x69, 0x6d, 0x61, 0x67, 0x65, 0x55, 0x72, 0x6c, 0x12, 0x14,
	0x0a, 0x05, 0x6c, 0x65, 0x76, 0x65, 0x6c, 0x18, 0x09, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x6c,
	0x65, 0x76, 0x65, 0x6c, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f,
	0x61, 0x74, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65,
	0x64, 0x41, 0x74, 0x22, 0x67, 0x0a, 0x1e, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72,
	0x6f, 0x75, 0x70, 0x49, 0x6d, 0x61, 0x67, 0x65, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x52, 0x65,
	0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x28, 0x0a, 0x10, 0x62, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73,
	0x5f, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52,
	0x0e, 0x62, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x49, 0x64, 0x12,
	0x1b, 0x0a, 0x09, 0x69, 0x6d, 0x61, 0x67, 0x65, 0x5f, 0x75, 0x72, 0x6c, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x08, 0x69, 0x6d, 0x61, 0x67, 0x65, 0x55, 0x72, 0x6c, 0x22, 0x91, 0x02, 0x0a,
	0x0c, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x12, 0x0e, 0x0a,
	0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a,
	0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d,
	0x65, 0x12, 0x19, 0x0a, 0x08, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20,
	0x01, 0x28, 0x03, 0x52, 0x07, 0x61, 0x64, 0x6d, 0x69, 0x6e, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a,
	0x67, 0x72, 0x6f, 0x75, 0x70, 0x5f, 0x69, 0x6e, 0x66, 0x6f, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x09, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x49, 0x6e, 0x66, 0x6f, 0x12, 0x21, 0x0a, 0x0c, 0x70,
	0x68, 0x6f, 0x6e, 0x65, 0x5f, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x18, 0x05, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x0b, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x12, 0x14,
	0x0a, 0x05, 0x65, 0x6d, 0x61, 0x69, 0x6c, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x65,
	0x6d, 0x61, 0x69, 0x6c, 0x12, 0x18, 0x0a, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x18,
	0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x12, 0x1b,
	0x0a, 0x09, 0x69, 0x6d, 0x61, 0x67, 0x65, 0x5f, 0x75, 0x72, 0x6c, 0x18, 0x08, 0x20, 0x01, 0x28,
	0x09, 0x52, 0x08, 0x69, 0x6d, 0x61, 0x67, 0x65, 0x55, 0x72, 0x6c, 0x12, 0x14, 0x0a, 0x05, 0x6c,
	0x65, 0x76, 0x65, 0x6c, 0x18, 0x09, 0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x6c, 0x65, 0x76, 0x65,
	0x6c, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x5f, 0x61, 0x74, 0x18,
	0x0a, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x41, 0x74,
	0x22, 0x93, 0x01, 0x0a, 0x11, 0x43, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x42, 0x72, 0x6f, 0x6b, 0x65,
	0x72, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x1d, 0x0a, 0x0a, 0x67, 0x72,
	0x6f, 0x75, 0x70, 0x5f, 0x69, 0x6e, 0x66, 0x6f, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09,
	0x67, 0x72, 0x6f, 0x75, 0x70, 0x49, 0x6e, 0x66, 0x6f, 0x12, 0x21, 0x0a, 0x0c, 0x70, 0x68, 0x6f,
	0x6e, 0x65, 0x5f, 0x6e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0b, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x12, 0x18, 0x0a, 0x07,
	0x61, 0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x61,
	0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x22, 0xbc, 0x01, 0x0a, 0x19, 0x43, 0x72, 0x65, 0x61, 0x74,
	0x65, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x19, 0x0a, 0x08, 0x61, 0x64, 0x6d, 0x69,
	0x6e, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x07, 0x61, 0x64, 0x6d, 0x69,
	0x6e, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x5f, 0x69, 0x6e, 0x66,
	0x6f, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x49, 0x6e,
	0x66, 0x6f, 0x12, 0x21, 0x0a, 0x0c, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x5f, 0x6e, 0x75, 0x6d, 0x62,
	0x65, 0x72, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x70, 0x68, 0x6f, 0x6e, 0x65, 0x4e,
	0x75, 0x6d, 0x62, 0x65, 0x72, 0x12, 0x14, 0x0a, 0x05, 0x65, 0x6d, 0x61, 0x69, 0x6c, 0x18, 0x05,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x05, 0x65, 0x6d, 0x61, 0x69, 0x6c, 0x12, 0x18, 0x0a, 0x07, 0x61,
	0x64, 0x64, 0x72, 0x65, 0x73, 0x73, 0x18, 0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x61, 0x64,
	0x64, 0x72, 0x65, 0x73, 0x73, 0x22, 0x70, 0x0a, 0x1b, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x42,
	0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x73, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x12, 0x3b, 0x0a, 0x0d, 0x62, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x5f,
	0x67, 0x72, 0x6f, 0x75, 0x70, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x16, 0x2e, 0x67, 0x65,
	0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72,
	0x6f, 0x75, 0x70, 0x52, 0x0c, 0x62, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72, 0x6f, 0x75,
	0x70, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x22, 0x27, 0x0a, 0x15, 0x42, 0x72, 0x6f, 0x6b, 0x65,
	0x72, 0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x49, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x02, 0x69, 0x64,
	0x22, 0x80, 0x01, 0x0a, 0x1a, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x42, 0x72, 0x6f, 0x6b, 0x65,
	0x72, 0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12,
	0x12, 0x0a, 0x04, 0x70, 0x61, 0x67, 0x65, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x04, 0x70,
	0x61, 0x67, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x12, 0x16, 0x0a, 0x06, 0x73, 0x65, 0x61,
	0x72, 0x63, 0x68, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73, 0x65, 0x61, 0x72, 0x63,
	0x68, 0x12, 0x20, 0x0a, 0x0c, 0x73, 0x6f, 0x72, 0x74, 0x5f, 0x62, 0x79, 0x5f, 0x64, 0x61, 0x74,
	0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x73, 0x6f, 0x72, 0x74, 0x42, 0x79, 0x44,
	0x61, 0x74, 0x65, 0x32, 0xbb, 0x04, 0x0a, 0x13, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47,
	0x72, 0x6f, 0x75, 0x70, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x5b, 0x0a, 0x12, 0x43,
	0x72, 0x65, 0x61, 0x74, 0x65, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72, 0x6f, 0x75,
	0x70, 0x12, 0x23, 0x2e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x43, 0x72, 0x65,
	0x61, 0x74, 0x65, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x1e, 0x2e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x2e, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52, 0x65,
	0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x54, 0x0a, 0x0f, 0x47, 0x65, 0x74, 0x42,
	0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x12, 0x1f, 0x2e, 0x67, 0x65,
	0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72,
	0x6f, 0x75, 0x70, 0x49, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x1e, 0x2e, 0x67,
	0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47,
	0x72, 0x6f, 0x75, 0x70, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x64,
	0x0a, 0x13, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47,
	0x72, 0x6f, 0x75, 0x70, 0x73, 0x12, 0x24, 0x2e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x2e, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72,
	0x6f, 0x75, 0x70, 0x73, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x25, 0x2e, 0x67, 0x65,
	0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x47, 0x65, 0x74, 0x41, 0x6c, 0x6c, 0x42, 0x72, 0x6f,
	0x6b, 0x65, 0x72, 0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x22, 0x00, 0x12, 0x53, 0x0a, 0x12, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x42, 0x72,
	0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x12, 0x1b, 0x2e, 0x67, 0x65, 0x6e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x43, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x42, 0x72, 0x6f, 0x6b,
	0x65, 0x72, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x1a, 0x1e, 0x2e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x2e, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x65, 0x0a, 0x17, 0x55, 0x70, 0x6c,
	0x6f, 0x61, 0x64, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x49,
	0x6d, 0x61, 0x67, 0x65, 0x12, 0x28, 0x2e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e,
	0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x49, 0x6d, 0x61, 0x67,
	0x65, 0x55, 0x70, 0x6c, 0x6f, 0x61, 0x64, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x1e,
	0x2e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2e, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72,
	0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00,
	0x12, 0x4f, 0x0a, 0x12, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72,
	0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x12, 0x1f, 0x2e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x2e, 0x42, 0x72, 0x6f, 0x6b, 0x65, 0x72, 0x73, 0x47, 0x72, 0x6f, 0x75, 0x70, 0x49, 0x64,
	0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x16, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65,
	0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x45, 0x6d, 0x70, 0x74, 0x79, 0x22,
	0x00, 0x42, 0x20, 0x5a, 0x1e, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x62, 0x72,
	0x6f, 0x6b, 0x65, 0x72, 0x73, 0x5f, 0x67, 0x72, 0x6f, 0x75, 0x70, 0x5f, 0x73, 0x65, 0x72, 0x76,
	0x69, 0x63, 0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_brokers_group_service_proto_rawDescOnce sync.Once
	file_brokers_group_service_proto_rawDescData = file_brokers_group_service_proto_rawDesc
)

func file_brokers_group_service_proto_rawDescGZIP() []byte {
	file_brokers_group_service_proto_rawDescOnce.Do(func() {
		file_brokers_group_service_proto_rawDescData = protoimpl.X.CompressGZIP(file_brokers_group_service_proto_rawDescData)
	})
	return file_brokers_group_service_proto_rawDescData
}

var file_brokers_group_service_proto_msgTypes = make([]protoimpl.MessageInfo, 9)
var file_brokers_group_service_proto_goTypes = []interface{}{
	(*BrokerInfo)(nil),                     // 0: genproto.BrokerInfo
	(*BrokersGroupResponse)(nil),           // 1: genproto.BrokersGroupResponse
	(*BrokersGroupImageUploadRequest)(nil), // 2: genproto.BrokersGroupImageUploadRequest
	(*BrokersGroup)(nil),                   // 3: genproto.BrokersGroup
	(*ChangeBrokerGroup)(nil),              // 4: genproto.ChangeBrokerGroup
	(*CreateBrokersGroupRequest)(nil),      // 5: genproto.CreateBrokersGroupRequest
	(*GetAllBrokersGroupsResponse)(nil),    // 6: genproto.GetAllBrokersGroupsResponse
	(*BrokersGroupIdRequest)(nil),          // 7: genproto.BrokersGroupIdRequest
	(*GetAllBrokersGroupsRequest)(nil),     // 8: genproto.GetAllBrokersGroupsRequest
	(*emptypb.Empty)(nil),                  // 9: google.protobuf.Empty
}
var file_brokers_group_service_proto_depIdxs = []int32{
	0, // 0: genproto.BrokersGroupResponse.admin_info:type_name -> genproto.BrokerInfo
	3, // 1: genproto.GetAllBrokersGroupsResponse.brokers_group:type_name -> genproto.BrokersGroup
	5, // 2: genproto.BrokersGroupService.CreateBrokersGroup:input_type -> genproto.CreateBrokersGroupRequest
	7, // 3: genproto.BrokersGroupService.GetBrokersGroup:input_type -> genproto.BrokersGroupIdRequest
	8, // 4: genproto.BrokersGroupService.GetAllBrokersGroups:input_type -> genproto.GetAllBrokersGroupsRequest
	4, // 5: genproto.BrokersGroupService.UpdateBrokersGroup:input_type -> genproto.ChangeBrokerGroup
	2, // 6: genproto.BrokersGroupService.UploadBrokersGroupImage:input_type -> genproto.BrokersGroupImageUploadRequest
	7, // 7: genproto.BrokersGroupService.DeleteBrokersGroup:input_type -> genproto.BrokersGroupIdRequest
	1, // 8: genproto.BrokersGroupService.CreateBrokersGroup:output_type -> genproto.BrokersGroupResponse
	1, // 9: genproto.BrokersGroupService.GetBrokersGroup:output_type -> genproto.BrokersGroupResponse
	6, // 10: genproto.BrokersGroupService.GetAllBrokersGroups:output_type -> genproto.GetAllBrokersGroupsResponse
	1, // 11: genproto.BrokersGroupService.UpdateBrokersGroup:output_type -> genproto.BrokersGroupResponse
	1, // 12: genproto.BrokersGroupService.UploadBrokersGroupImage:output_type -> genproto.BrokersGroupResponse
	9, // 13: genproto.BrokersGroupService.DeleteBrokersGroup:output_type -> google.protobuf.Empty
	8, // [8:14] is the sub-list for method output_type
	2, // [2:8] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_brokers_group_service_proto_init() }
func file_brokers_group_service_proto_init() {
	if File_brokers_group_service_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_brokers_group_service_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BrokerInfo); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_brokers_group_service_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BrokersGroupResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_brokers_group_service_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BrokersGroupImageUploadRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_brokers_group_service_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BrokersGroup); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_brokers_group_service_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ChangeBrokerGroup); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_brokers_group_service_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateBrokersGroupRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_brokers_group_service_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetAllBrokersGroupsResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_brokers_group_service_proto_msgTypes[7].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*BrokersGroupIdRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_brokers_group_service_proto_msgTypes[8].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetAllBrokersGroupsRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_brokers_group_service_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   9,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_brokers_group_service_proto_goTypes,
		DependencyIndexes: file_brokers_group_service_proto_depIdxs,
		MessageInfos:      file_brokers_group_service_proto_msgTypes,
	}.Build()
	File_brokers_group_service_proto = out.File
	file_brokers_group_service_proto_rawDesc = nil
	file_brokers_group_service_proto_goTypes = nil
	file_brokers_group_service_proto_depIdxs = nil
}
